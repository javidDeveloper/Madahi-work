package sattar.javid.madahi_work.MainMVP.view;

import android.support.annotation.IdRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabSelectListener;

import sattar.javid.madahi_work.R;


public class FirstActivity extends AppCompatActivity {
    private BottomBar bottomBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);
        init();

        bottomBar.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelected(@IdRes int tabId) {
                if (tabId == R.id.tab_exit) {
                }
            }
        });

    }
    private void init() {
        bottomBar=findViewById(R.id.bottomBar);
    }



}
