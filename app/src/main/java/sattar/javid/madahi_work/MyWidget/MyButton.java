package sattar.javid.madahi_work.MyWidget;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;

/**
 * Created by maboudi on 10/03/2018.
 */

public class MyButton extends AppCompatButton {
    public MyButton(Context context) {
        super(context);
        typeYekan();
    }

    public MyButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        typeYekan();
    }
    public void typeYekan(){
        Typeface face = Typeface.createFromAsset(getContext().getAssets(),
                "font/yekan.ttf");
        setTypeface(face);
    }
}
